package com.isivkov.playitnow;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.util.Log;

public class FileInfoStorage {

	private static final String TAG = "FileInfoStorage";

	Map<String, String> uriToLocal = new HashMap<String, String>();
	private Context context;

	public FileInfoStorage(Context context) {
		super();
		this.context = context;

		load();
	}

	public void putLocalUri(FileListItem item) {
		uriToLocal.put(item.getUri().toString(), item.getLocalUri().toString());

		save();
	}

	public String getLocalUri(FileListItem item) {
		return uriToLocal.get(item.getUri().toString());
	}

	private void load() {
		FileInputStream fis;
		try {
			fis = context.openFileInput("fileinfo.txt");
		} catch (FileNotFoundException e) {
			Log.w(TAG, "URI storage file not found");
			return;
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fis));

		try {
			String line;
			String uri = null;
			boolean oddLine = true;
			while ((line = br.readLine()) != null) {
				if (oddLine) {
					uri = line;
				} else {
					uriToLocal.put(uri, line);
					Log.d(TAG, String.format("Load: %s\n%s", uri, line));
				}
				oddLine = !oddLine;
			}
		} catch (IOException e) {
			Log.e(TAG, "Can't read URI storage file", e);
		}
		try {
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Log.i(TAG, String.format("Loaded %d files", uriToLocal.size()));
	}

	private void save() {
		StringBuilder sb = new StringBuilder();

		for (String uri : uriToLocal.keySet()) {
			sb.append(uri).append("\n");
			sb.append(uriToLocal.get(uri)).append("\n");
		}

		FileOutputStream output = null;
		try {
			output = context.openFileOutput("fileinfo.txt",
					Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			Log.e(TAG, "Can't open URI storage file", e);
			return;
		}

		try {
			output.write(sb.toString().getBytes());
		} catch (IOException e) {
			Log.e(TAG, "Can't write URI storage file", e);
		}
		try {
			output.close();
		} catch (IOException e) {
			Log.e(TAG, "Can't close URI storage file", e);
		}
	}
}
