package com.isivkov.playitnow;

import android.net.Uri;

public class FileListItem {

	private boolean playing;
	private Uri uri;
	private Uri localUri;
	private	Status status;
	private String title;
	private String album;
	private String artist;
	private String duration;
	private int progress;
	
	public enum Status {Pending, Running, Ready, Failed, Playing};
	
	public FileListItem(String uri) {
		super();
		this.playing = false;
		this.uri = Uri.parse(uri);
		this.status = Status.Pending;
	}
	public boolean isPlaying() {
		return playing;
	}
	public void setPlaying(boolean playing) {
		this.playing = playing;
	}
	public Uri getUri() {
		return uri;
	}
	public void setUri(Uri uri) {
		this.uri = uri;
	}
	public Uri getLocalUri() {
		return localUri;
	}
	public void setLocalUri(Uri localUri) {
		this.localUri = localUri;
	}
	public boolean exists() {
		return localUri != null;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
}
