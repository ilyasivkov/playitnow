package com.isivkov.playitnow;

import android.net.Uri;

public interface FileStatusHolder {

	public abstract void start(Uri uri);

	public abstract void failed(Uri uri);

	public abstract void stop(Uri uri);

	public abstract void done(Uri uri);

	public abstract void play(Uri uri);

	public abstract void setProgress(Uri uri, int progress);

}