package com.isivkov.playitnow;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.media.MediaMetadataRetriever;
import static android.media.MediaMetadataRetriever.*;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.isivkov.playitnow.FileDownloader.FileReadyListener;
import com.isivkov.playitnow.FileListItem.Status;

public class MainActivity extends FragmentActivity implements FileReadyListener {

	private static final String TAG = "MainActivity";
	private List<FileListItem> files = new ArrayList<FileListItem>();
	private FileListAdapter fileListAdapter;
	private FileInfoStorage fis;
	private FileDownloader fileDownloader;
	private PlayerServiceConnection connection;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		fis = new FileInfoStorage(this);

		fileListAdapter = new FileListAdapter(this, files);

		fileDownloader = new FileDownloader(this);
		fileDownloader.setListener(this);
		fileDownloader.setStatusHolder(fileListAdapter);

		connection = new PlayerServiceConnection(fileListAdapter);
		bindService(new Intent(this, PlayerService.class), connection,
				BIND_AUTO_CREATE);

		if (savedInstanceState == null) {
			FilesFragment filesFragment = new FilesFragment();
			filesFragment.setListAdapter(fileListAdapter);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.lt_main, filesFragment).commit();
		}

		readFileList();

		downloadFiles();
	}

	@Override
	protected void onDestroy() {
		if (connection.isBound()) {
			unbindService(connection);
		}
		super.onDestroy();
	}

	private void downloadFiles() {
		for (FileListItem file : files) {
			if (file.getStatus().equals(Status.Pending)) {
				fileDownloader.downloadFile(file.getUri());
			}
		}
	}

	private void readFileList() {
		File sdcard = Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
		File file = new File(sdcard, "files.txt");
		if (!file.exists()) {
			Log.e(TAG,
					String.format("File %s not exists", file.getAbsolutePath()));
			return;
		}

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null) {
				FileListItem item = new FileListItem(line);
				files.add(item);
				String localUri = fis.getLocalUri(item);
				if (localUri == null) {
					Log.w(TAG, String.format("No local URI found for %s", item
							.getUri()));
					continue;
				}
				item.setLocalUri(Uri.parse(localUri));
				fillMetadata(item);
				fileListAdapter.done(item.getUri());

			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public class FilesFragment extends ListFragment {

		@Override
		public void onListItemClick(ListView l, View v, int position, long id) {
			FileListItem item = (FileListItem) getListView().getItemAtPosition(
					position);
			if (item.getStatus().equals(Status.Ready)) {
				connection.getPlayerService().stop();
				connection.getPlayerService().play(item.getLocalUri());
			} else {
				if (item.getStatus().equals(Status.Playing)) {
					connection.getPlayerService().stop();
				}
			}
		}

	}

	@Override
	public void onFileReady(Uri uri, Uri localUri) {
		FileListItem item = fileListAdapter.findFileByUri(uri);
		if (item == null) {
			return;
		}
		item.setLocalUri(localUri);
		fis.putLocalUri(item);

		fillMetadata(item);

	}

	private void fillMetadata(FileListItem item) {
		Map<Integer, String> meta = getFileTitle(item);

		item.setAlbum(emptyToNull(meta.get(METADATA_KEY_ALBUM)));
		item.setArtist(emptyToNull(meta.get(METADATA_KEY_ARTIST)));
		item.setDuration(emptyToNull(meta.get(METADATA_KEY_DURATION)));
		item.setTitle(emptyToNull(meta.get(METADATA_KEY_TITLE)));
	}

	private String emptyToNull(String string) {
		if (string == null) {
			return null;
		}
		if (string.isEmpty()) {
			return null;
		}
		return string;
	}

	private Map<Integer, String> getFileTitle(FileListItem item) {
		Map<Integer, String> result = new HashMap<Integer, String>();

		MediaMetadataRetriever mmr = new MediaMetadataRetriever();

		String uri = item.getLocalUri().toString();
		String cuttedUri;
		try {
			cuttedUri = URLDecoder.decode(uri, "UTF-8").replaceFirst("file://",
					"");
			mmr.setDataSource(cuttedUri);
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "Decoding error for " + uri, e);
			return result;
		} catch (IllegalArgumentException e) {
			Log.e(TAG, "Metadata extraction error for " + uri, e);
			return result;
		}

		List<Integer> keys = new ArrayList<Integer>();
		keys.add(METADATA_KEY_ALBUM);
		keys.add(METADATA_KEY_ARTIST);
		keys.add(METADATA_KEY_DURATION);
		keys.add(METADATA_KEY_TITLE);

		for (Integer key : keys) {
			result.put(key, mmr.extractMetadata(key));
		}

		return result;
	}

}
