package com.isivkov.playitnow;

import static android.app.DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR;
import static android.app.DownloadManager.COLUMN_ID;
import static android.app.DownloadManager.COLUMN_LOCAL_URI;
import static android.app.DownloadManager.COLUMN_STATUS;
import static android.app.DownloadManager.COLUMN_TOTAL_SIZE_BYTES;
import static android.app.DownloadManager.COLUMN_URI;
import static android.app.DownloadManager.STATUS_FAILED;
import static android.app.DownloadManager.STATUS_PAUSED;
import static android.app.DownloadManager.STATUS_RUNNING;
import static android.app.DownloadManager.STATUS_SUCCESSFUL;

import java.util.HashSet;
import java.util.Set;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

public class FileDownloader {
	private static final String TAG = "FileDownloader";
	private DownloadManager downloadManager;
	private long downloadId;
	private Context context;
	private FileStatusHolder statusHolder;
	private FileReadyListener listener;
	private ProgressRunnable progressMonitor;
	private Set<Long> ids = new HashSet<Long>();

	public interface FileReadyListener {
		public void onFileReady(Uri uri, Uri localUri);
	}

	public FileDownloader(Context context) {
		this.context = context;

		progressMonitor = new ProgressRunnable();

		downloadManager = (DownloadManager) context
				.getSystemService(Context.DOWNLOAD_SERVICE);

	}

	public void downloadFile(Uri uri) {
		DownloadManager.Request request = new DownloadManager.Request(uri);

		request.setTitle("My Data Download");
		request.setDescription(uri.getLastPathSegment());
		request.setDestinationInExternalFilesDir(context,
				Environment.DIRECTORY_DOWNLOADS, uri.getLastPathSegment());

		ids.add(downloadManager.enqueue(request));

		progressMonitor.start();
		statusHolder.start(uri);
		Log.d(TAG, String.format("Starting with #%d from %s", downloadId, uri));
	}

	public void onDownloadSuccessful(Uri uri, Uri localUri) {
		if (getListener() != null) {
			getListener().onFileReady(uri, localUri);
		}

		getStatusHolder().done(uri);
		Log.d(TAG, String.format("Download %s completed", localUri));
	}

	public void onDownloadFailed(Uri uri) {
		getStatusHolder().failed(uri);
		Log.d(TAG, String.format("Download %s failed", uri));
	}

	public FileStatusHolder getStatusHolder() {
		return statusHolder;
	}

	public void setStatusHolder(FileStatusHolder statusHolder) {
		this.statusHolder = statusHolder;
	}

	public FileReadyListener getListener() {
		return listener;
	}

	public void setListener(FileReadyListener listener) {
		this.listener = listener;
	}

	private class ProgressRunnable implements Runnable {

		private static final int INTERVAL = 1000;

		private Handler handler;

		public ProgressRunnable() {
			handler = new Handler();
		}

		private void start() {
			handler.postDelayed(this, INTERVAL);
		}

		@Override
		public void run() {
			DownloadManager.Query q = new DownloadManager.Query();

			Cursor c = downloadManager.query(q);

			if (!c.moveToFirst()) {
				Log.i(TAG, "No active downloads");
				return;
			}

			do {
				long id = c.getLong(c.getColumnIndex(COLUMN_ID));
				if (ids.contains(id)) {
					processDownload(c);
				}
			} while (c.moveToNext());
			
			c.close();

			start();
		}

		private void processDownload(Cursor c) {
			long id = c.getLong(c.getColumnIndex(COLUMN_ID));
			Uri uri = Uri.parse(c.getString(c.getColumnIndex(COLUMN_URI)));

			switch (c.getInt(c.getColumnIndex(COLUMN_STATUS))) {
			case STATUS_SUCCESSFUL:
				Uri localUri = Uri.parse(c.getString(c
						.getColumnIndex(COLUMN_LOCAL_URI)));
				onDownloadSuccessful(uri, localUri);
				ids.remove(id);
				break;
			case STATUS_FAILED:
				onDownloadFailed(uri);
				ids.remove(id);
				break;
			case STATUS_PAUSED:
				onDownloadFailed(uri);
				ids.remove(id);
				break;
			case STATUS_RUNNING:
				sendProgress(c);
				break;
			}
		}

		private void sendProgress(Cursor c) {
			int ciDownloaded = c.getColumnIndex(COLUMN_BYTES_DOWNLOADED_SO_FAR);
			int ciTotal = c.getColumnIndex(COLUMN_TOTAL_SIZE_BYTES);

			int bytes_downloaded = c.getInt(ciDownloaded);
			int bytes_total = c.getInt(ciTotal);
			
			Uri uri = Uri.parse(c.getString(c.getColumnIndex(COLUMN_URI)));

			int progress;
			if (bytes_total == 0) {
				progress = 100;
			} else {
				progress = (int) ((bytes_downloaded * 100L) / bytes_total);
			}

			statusHolder.setProgress(uri, progress);
		}
	}
}
