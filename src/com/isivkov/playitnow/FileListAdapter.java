package com.isivkov.playitnow;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.isivkov.playitnow.FileListItem.Status;

public class FileListAdapter extends ArrayAdapter<FileListItem> implements
		FileStatusHolder {

	private static final String TAG = "FileListAdapter";

	public FileListAdapter(Context context, List<FileListItem> items) {
		super(context, R.layout.filelist_item, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;

		if (convertView == null) {
			// inflate the GridView item layout
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.filelist_item, parent,
					false);

			// initialize the view holder
			viewHolder = new ViewHolder();
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.tvTitle);
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.tvTime);
			viewHolder.tvAlbum = (TextView) convertView
					.findViewById(R.id.tvAlbum);
			viewHolder.tvArtist = (TextView) convertView
					.findViewById(R.id.tvArtist);
			viewHolder.pbProgress = (ProgressBar) convertView
					.findViewById(R.id.pbDownload);
			viewHolder.statusViews.put(Status.Failed,
					(ImageView) convertView.findViewById(R.id.ivFailed));
			viewHolder.statusViews.put(Status.Pending,
					(ImageView) convertView.findViewById(R.id.ivPending));
			viewHolder.statusViews.put(Status.Playing,
					(ImageView) convertView.findViewById(R.id.ivPause));
			viewHolder.statusViews.put(Status.Ready,
					(ImageView) convertView.findViewById(R.id.ivPlay));
			viewHolder.statusViews.put(Status.Running,
					(ImageView) convertView.findViewById(R.id.ivRunning));
			convertView.setTag(viewHolder);
		} else {
			// recycle the already inflated view
			viewHolder = (ViewHolder) convertView.getTag();
		}

		// update the item view
		FileListItem item = getItem(position);
		Status st = item.getStatus();
		for (Status s : viewHolder.statusViews.keySet()) {
			viewHolder.statusViews.get(s).setVisibility(
					s.equals(st) ? View.VISIBLE : View.INVISIBLE);
		}

		viewHolder.pbProgress.setProgress(item.getProgress());
		viewHolder.pbProgress.setIndeterminate(item.getProgress() < 0);
		viewHolder.pbProgress.setVisibility(b2v(Status.Running.equals(st)));

		boolean exists = Status.Ready.equals(st) || Status.Playing.equals(st);

		viewHolder.tvAlbum.setVisibility(b2v(exists));
		viewHolder.tvArtist.setVisibility(b2v(exists));
		viewHolder.tvTime.setVisibility(b2v(exists));

		viewHolder.tvTitle.setText(replaceNull(item.getTitle(), item.getUri()
				.toString()));
		viewHolder.tvAlbum
				.setText(replaceNull(item.getAlbum(), "Unknown album"));
		viewHolder.tvTime
				.setText(replaceNull(formatDuration(item.getDuration()), ""));
		viewHolder.tvArtist.setText(replaceNull(item.getArtist(),
				"Unknown artist"));

		return convertView;
	}

	private String formatDuration(String duration) {
		if(duration == null) {
		return null;
		}
		
		long ms = Long.parseLong(duration);
		long h = TimeUnit.MILLISECONDS.toHours(ms);
		long m = TimeUnit.MILLISECONDS.toMinutes(ms);
		long s = TimeUnit.MILLISECONDS.toSeconds(ms);
		
		if(h > 0) {
			return String.format("%d:%02d:%02d", h,m,s);
		}
		return String.format("%02d:%02d", m,s);
	}

	private int b2v(boolean value) {
		return value ? View.VISIBLE : View.INVISIBLE;
	}

	private CharSequence replaceNull(String value, String ifNull) {
		return value == null ? ifNull : value;
	}

	/**
	 * The view holder design pattern prevents using findViewById() repeatedly
	 * in the getView() method of the adapter.
	 * 
	 * @see http 
	 *      ://developer.android.com/training/improving-layouts/smooth-scrolling
	 *      .html#ViewHolder
	 */
	private static class ViewHolder {
		TextView tvTitle;
		TextView tvTime;
		TextView tvAlbum;
		TextView tvArtist;
		ProgressBar pbProgress;
		Map<Status, ImageView> statusViews = new HashMap<Status, ImageView>();
	}

	@Override
	public void start(Uri uri) {
		updateStatus(uri, Status.Running);
	}

	@Override
	public void failed(Uri uri) {
		updateStatus(uri, Status.Failed);
	}

	@Override
	public void stop(Uri uri) {
		updateStatus(uri, Status.Ready);
	}

	@Override
	public void done(Uri uri) {
		updateStatus(uri, Status.Ready);
	}

	@Override
	public void play(Uri uri) {
		updateStatus(uri, Status.Playing);
	}

	private void updateStatus(Uri uri, Status status) {
		FileListItem item = findFileByUri(uri);
		if (item != null) {
			item.setStatus(status);
			notifyDataSetChanged();
		}
	}

	public FileListItem findFileByUri(Uri uri) {
		for (int i = 0; i < getCount(); i += 1) {
			FileListItem item = getItem(i);
			if (item.getUri().equals(uri)) {
				return item;
			}
		}
		Log.w(TAG, "File for URI " + uri + " not found");
		return null;
	}

	public FileListItem findFileByLocalUri(Uri uri) {
		for (int i = 0; i < getCount(); i += 1) {
			FileListItem item = getItem(i);
			if (uri.equals(item.getLocalUri())) {
				return item;
			}
		}
		Log.w(TAG, "File for local URI " + uri + " not found");
		return null;
	}

	@Override
	public void setProgress(Uri uri, int progress) {
		FileListItem item = findFileByUri(uri);
		if (item == null) {
			return;
		}
		item.setProgress(progress);
		item.setStatus(progress > 0 ? Status.Running : Status.Pending);
		notifyDataSetChanged();
	}
}
