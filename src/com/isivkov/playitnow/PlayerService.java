package com.isivkov.playitnow;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class PlayerService extends Service implements OnPreparedListener,
		OnCompletionListener {

	private static final String TAG = "PlayerService";
	private PlayerBinder binder = new PlayerBinder();
	private MediaPlayer player;
	private PlaybackStatusListener statusListener = null;
	private Uri uri;

	public interface PlaybackStatusListener {
		public void onPlaybackStarted(Uri uri);

		public void onPlaybackStopped(Uri uri);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		player = new MediaPlayer();
		player.setOnPreparedListener(this);
		player.setOnCompletionListener(this);
	}

	@Override
	public void onDestroy() {
		player.release();
		player = null;
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return binder;
	}

	public class PlayerBinder extends Binder {
		PlayerService getService() {
			return PlayerService.this;
		}
	}

	public void play(Uri uri) {
		Log.d(TAG, "play()" + uri);
		if(player.isPlaying()) {
			Log.w(TAG, "Attempt to play when playing");
			return;
		}
		String uriStr;
		try {
			uriStr = URLDecoder.decode(uri.toString(), "UTF-8");
													} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			return;
		}
		Log.d(TAG, "Decoded URI is" + uriStr);
		String noProtocolUri = uriStr.replaceFirst("file://", "");
		try {
			player.reset();			
			player.setDataSource(noProtocolUri);
			player.prepareAsync();
			this.uri = uri;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stop() {
		Log.d(TAG, "stop()");
		if(!player.isPlaying()) {
			Log.w(TAG, "Attempt to stop when not playing");
			return;
		}
		player.stop();
		if (statusListener != null) {
			statusListener.onPlaybackStopped(uri);
		}
	}

	@Override
	public void onPrepared(MediaPlayer arg0) {
		Log.d(TAG, "Media prepared");
		player.start();
		if (statusListener != null) {
			statusListener.onPlaybackStarted(uri);
		}
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		Log.d(TAG, "Playback completed");
		if (statusListener != null) {
			statusListener.onPlaybackStopped(uri);
		}
	}

	public PlaybackStatusListener getStatusListener() {
		return statusListener;
	}

	public void setStatusListener(PlaybackStatusListener statusListener) {
		this.statusListener = statusListener;
	}
}
