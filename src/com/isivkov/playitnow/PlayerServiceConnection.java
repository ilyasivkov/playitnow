package com.isivkov.playitnow;

import com.isivkov.playitnow.PlayerService.PlaybackStatusListener;
import com.isivkov.playitnow.PlayerService.PlayerBinder;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

public class PlayerServiceConnection implements ServiceConnection, PlaybackStatusListener {
	private static final String TAG = "PlayerServiceConnection";
	private FileListAdapter fileListAdapter;
	private boolean bound = false;
	private PlayerService playerService;

	public PlayerServiceConnection(FileListAdapter adapter) {
		super();
		fileListAdapter = adapter;
	}

	@Override
	public void onServiceConnected(ComponentName className, IBinder service) {
		playerService = ((PlayerBinder) service).getService();
		playerService.setStatusListener(this);
		bound = true;
		Log.i(TAG, "Player has been bound");
	}

	@Override
	public void onServiceDisconnected(ComponentName arg0) {
		bound = false;
		Log.i(TAG, "Player has been unbound");
	}

	@Override
	public void onPlaybackStarted(Uri localUri) {
		fileListAdapter.play(localToUri(localUri));
	}

	@Override
	public void onPlaybackStopped(Uri localUri) {
		fileListAdapter.stop(localToUri(localUri));
	}

	private Uri localToUri(Uri localUri) {
		return fileListAdapter.findFileByLocalUri(localUri).getUri();
	}

	public boolean isBound() {
		return bound;
	}

	public PlayerService getPlayerService() {
		return playerService;
	}
}
